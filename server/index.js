const Path = require('path');
const Hapi = require('hapi');
const Webpack = require('webpack');
const DashboardPlugin = require('webpack-dashboard/plugin');
const Config = require('../webpack.config.js');

const server = new Hapi.Server();
const host = 'localhost';
const port = 3000;
server.connection({ host, port });

const compiler = Webpack(Config);
compiler.apply(new DashboardPlugin());

const devMiddleware = require('webpack-dev-middleware')(compiler, {
    host,
    port,
    historyApiFallback: true,
    publicPath: Config.output.publicPath,
    quiet: true
});


const hotMiddleware = require('webpack-hot-middleware')(compiler, {
    log: () => {}
});

server.ext('onRequest', (req, reply) => {
    devMiddleware(req.raw.req, req.raw.res, (devError) => {
        if (devError) {
            return reply(devError);
        }
        return reply.continue();
    });
});

server.ext('onRequest', (req, reply) => {
    hotMiddleware(req.raw.req, req.raw.res, (err) => {
        if (err) {
            return reply(err);
        }
        return reply.continue();
    });
});

server.ext('onPreResponse', (req, reply) => {
    // assumes you are using html-webpack-plugin
    // if server static files, just reply witrh that file directly
    const filename = Path.join(compiler.outputPath, 'index.html');
    compiler.outputFileSystem.readFile(filename, (fileReadErr, result) => {
        if (fileReadErr) {
            return reply(fileReadErr);
        }
        reply(result).type('text/html');
    });
});

server.start(err => {
    if (err) {
        throw err;
    }
});
