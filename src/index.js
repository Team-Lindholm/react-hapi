import React from 'react';
import ReactDOM from 'react-dom';

const App = () => (
    <div>
        <h1 style={{ fontSize: '24px' }}>🍩</h1>
        <h2>Starter React/Hapi</h2>
        <h3>foobar 2</h3>
    </div>
);

const renderOnLoad = () => {
    ReactDOM.render(<App/>, document.getElementById('root'));
};

renderOnLoad();

module.hot.accept(() => {
    renderOnLoad();
});
